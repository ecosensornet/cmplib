#include <Wire.h>

#include "component_mcp3424.h"

MCP3424 adc = MCP3424();

#define I2C_SDA 12
#define I2C_SCL 11

TwoWire i2c = TwoWire(0);


void setup(void) {
  Serial.begin(115200);
  i2c.setPins(I2C_SDA, I2C_SCL);
  i2c.begin();

  adc.begin(i2c);
  delay(1); // MC3424 needs 300us to settle, wait 1ms

  // Check device present
  if (!adc.is_connected()) {
    Serial.print("No device found at address ");
    Serial.println(adc.address(), HEX);
    while (true) {
      Serial.println("stuck no device");
      delay(1000);
    }
  }

}

void loop(void) {
  uint8_t err;
  long value = 0;
  MCP3424::Config status;

  err = adc.convert_and_read(MCP3424::chn_2, MCP3424::one_shot, MCP3424::res_18, MCP3424::gain_8, 1000000, value, status);
  if (err) {
    Serial.println("Convert error: " + err);
  }
  else {
    Serial.println("ADC: " + String(value));
  }

  delay(1000);
}
