from pathlib import Path
from shutil import copy

# copy all arduino library in library path
lib_pth = Path("~/Documents/Arduino/libraries/").expanduser()

root_dir = Path("src")

for sdir in root_dir.glob("*/"):
    srcs = tuple(sdir.glob("*.h")) + tuple(sdir.glob("*.cpp"))
    if len(srcs) > 0:
        tgt = lib_pth / f"cmplib_{sdir.name}"
        tgt.mkdir(exist_ok=True)
        for pth in srcs:
            print("copy", pth, tgt / pth.name)
            copy(pth, tgt / pth.name)
