#include <Wire.h>

#include "component_mcp3421.h"

/**
  @brief Constructor
*/
MCP3421::MCP3421(const uint8_t address) : _address(address) {}

/**
  @brief Initialize device
  @param [in] config Configure Parameter
*/
void MCP3421::begin(TwoWire& wirePort) {
  _wire = &wirePort;
}


bool MCP3421::is_connected() {
  _wire->requestFrom(_address, (uint8_t)1);
  return _wire->available();
}


/**
  @brief Configure device
  @param [in] config Configure Parameter
*/
void MCP3421::configure(const Mode mode, const Resolution resolution, const Gain gain) {
  _cfg.reg  = 0x00;
  _cfg.bit.GAIN = ((uint8_t)gain & 0x03);
  _cfg.bit.RES = ((uint8_t)resolution & 0x03);
  _cfg.bit.OC = (uint8_t)mode;

  _write_i2c(_cfg.reg);
}

/**
  @brief Check if next value is ready
  @param [out] bool true if ready or false if not
*/
bool MCP3421::is_ready() {
  if (_read_i2c() < 0)
    return false;

  return (_status.bit.RDY == 0) ? true : false;
}

/**
  @brief Trigger one conversion
*/
void MCP3421::trigger() {
  _write_i2c(_cfg.reg | 0x80);
}


/**
  @brief Formatted value taking gain and resolution into consideration
*/
float MCP3421::value() {
  float _val = _raw_value;
  switch (_cfg.bit.RES) {
    case res_12:
      _val *= 1;
      break;
    case res_14:
      _val *= 0.25;
      break;
    case res_16:
      _val *= 0.0625;
      break;
    case res_18:
      _val *= 0.015625;
      break;
  }

  switch (_cfg.bit.GAIN) {
    case gain_1:
      _val /= 1;
      break;
    case gain_2:
      _val /= 2;
      break;
    case gain_4:
      _val /= 4;
      break;
    case gain_8:
      _val /= 8;
      break;
  }
  return _val;
}

/**
  @brief Read I2C
  @param [out] 0 on success or -1 on error
*/
int MCP3421::_read_i2c() {
  uint8_t u8Data;
  uint8_t u8Len = (_cfg.bit.RES == res_18) ? 4 : 3;

  if ((u8Len != _wire->requestFrom(_address, u8Len)) ||
      (u8Len < 3))
    return -1;

  u8Data     = (uint8_t)_wire->read();
  _raw_value = ((u8Data & 0x80) != 0) ? -1 : 0;
  _raw_value = (_raw_value & 0xFFFFFF00) | u8Data;

  for (u8Len--; u8Len > 1; u8Len--)
  {
    _raw_value <<= 8;
    _raw_value  |= (uint8_t)_wire->read();
  }

  _status.reg = (uint8_t)_wire->read();
  return 0;
}

/**
  @brief Write I2C
  @param [in] value write data
*/
void MCP3421::_write_i2c(uint8_t data)
{
  _wire->begin();
  _wire->beginTransmission(_address);
  _wire->write(data);
  _wire->endTransmission();
}
