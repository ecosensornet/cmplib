#include <Wire.h>

#include "component_mcp3421.h"

MCP3421 adc = MCP3421();

#define I2C_SDA 12
#define I2C_SCL 11

TwoWire i2c = TwoWire(0);


void setup() {
  Serial.begin(115200);
  i2c.setPins(I2C_SDA, I2C_SCL);
  i2c.begin();

  adc.begin(i2c);
  // Check device present
  if (!adc.is_connected()) {
    Serial.print("No device found at address ");
    Serial.println(adc.address(), HEX);
    while (true) {
      yield();
    }
  }

  Serial.println("Init");
  adc.configure(MCP3421::one_shot, MCP3421::res_18, MCP3421::gain_1);
  adc.trigger();

}

void loop() {
  int32_t raw_val;
  float val;

  if (adc.is_ready()) {
    raw_val = adc.raw_value();
    val = adc.value();
    Serial.printf("\nADC: 0x%08lX (%ld) -> %f [mV]\n", raw_val, raw_val, val);
    adc.trigger();
  }
  else {
    Serial.printf(".");
  }
  delay(1000);
}
