#include <Wire.h>

#include "component_ltc2451.h"

LTC2451 adc = LTC2451();

#define I2C_SDA 11
#define I2C_SCL 9

TwoWire i2c = TwoWire(0);


void setup(void) {
  Serial.begin(115200);
  i2c.setPins(I2C_SDA, I2C_SCL);
  i2c.begin();
  delay(300);
  while (!Serial) {
    delay(100);
  }

  Serial.print("begin: ");
  Serial.println(adc.begin(i2c));

  // Check device present
  if (!adc.is_connected()) {
    Serial.print("No device found at address ");
    Serial.println(adc.address(), HEX);
    while (1)
      ;
  }
}

void loop(void) {
  Serial.println("Loop");

  uint16_t value = 0;
  adc.convert();
  while (!adc.sample_ready()) {
    Serial.println("Waiting");
    delay(100);
  }

  bool err = adc.read(value);
  if (err) {
    Serial.println("Convert error");
  } else {
    Serial.println("ADC: " + String(value));
  }

  delay(5000);
}
