#ifndef COMPONENT_LTC2451_H
#define COMPONENT_LTC2451_H

#include <Arduino.h>
#include <Wire.h>

class LTC2451 {
public:
  static const uint8_t DefaultAddress = 0x14;

  LTC2451(const uint8_t address = DefaultAddress);

  bool begin(TwoWire &wirePort = Wire);
  bool is_connected() const;

  /** Return the I2C address used for communicating with this device.
    */
  uint8_t address(void) const {
    return _address;
  }

  /** Launch new conversion.
    */
  void convert();

  /** Test whether conversion has finished.
    */
  bool sample_ready();

  /** Read the sample value from the device.
    */
  bool read(uint16_t &result) const;

private:
  uint8_t _address;
  TwoWire *_wire;
  uint16_t _last_meas;
};

#endif  // COMPONENT_LTC2451_H
